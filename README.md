# Plant Leaf Images generated using DCGAN

Tensorflow implementation of DCGAN to generate images.

Code used to generate images is found in : https://github.com/MahletK/DCGAN-tensorflow

## Images

These are images genrated using DCGAN.

The images are 100x100 pixels.

Each class contains 1550 images.

Orginal images are found here : https://github.com/spMohanty/PlantVillage-Dataset/tree/master/raw/color


### Usage of the code

git clone https://github.com/MahletK/DCGAN-tensorflow

cd DCGAN-tensorflow

Run ./run.sh file. Change the folder name to the directory where your images are located.

Generated images are stored in Samples folder.




